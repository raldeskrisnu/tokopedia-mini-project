package com.tokopedia.testproject.problems.news.network;

import com.tokopedia.testproject.problems.news.model.NewsResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsService {

    @GET("everything")
    Observable<NewsResult> getEverythingService(@Query("q") String keyword, @Query("pageSize") int page, @Query("sortBy") String sortBy);

    @GET("top-headlines")
    Observable<NewsResult> getTopNewsService(@Query("q") String keyword, @Query("pageSize") int page);

}
