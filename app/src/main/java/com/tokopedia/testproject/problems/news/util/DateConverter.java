package com.tokopedia.testproject.problems.news.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

    public void DateConverter(){

    }

    public String DateConverter(String date){
        Date d = null;
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");

        try
        {
            d = input.parse(date);
            String format = output.format(d);
            return format;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return "";
    }
}
