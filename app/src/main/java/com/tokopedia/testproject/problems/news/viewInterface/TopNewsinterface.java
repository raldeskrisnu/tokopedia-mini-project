package com.tokopedia.testproject.problems.news.viewInterface;

import com.tokopedia.testproject.problems.news.model.Articles;

import java.util.List;

public interface TopNewsinterface {

    void onSuccessTopNews(List<Articles> onSuccessTopNewsArticle);

    void onErrorTopNews(Throwable throwable);

    void onLoading();
}
