package com.tokopedia.testproject.problems.news.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsResult {

    @SerializedName("status")
    String status;

    @SerializedName("totalResults")
    Integer totalResults;

    @SerializedName("articles")
    List<Articles> articlesList;

    public String getStatus(){
        return status;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public List<Articles> getArticlesList() {
        return articlesList;
    }
}
