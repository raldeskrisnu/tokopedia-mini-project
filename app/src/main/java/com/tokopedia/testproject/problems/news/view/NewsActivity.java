package com.tokopedia.testproject.problems.news.view;

import android.os.Bundle;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;
import com.tokopedia.testproject.MainActivity;
import com.tokopedia.testproject.R;
import com.tokopedia.testproject.problems.news.model.Articles;
import com.tokopedia.testproject.problems.news.presenter.NewsPresenter;
import com.tokopedia.testproject.problems.news.util.CheckConnectionInternet;
import com.tokopedia.testproject.problems.news.viewInterface.TopNewsinterface;
import com.tokopedia.testproject.problems.news.viewInterface.ViewInterface;

import java.util.List;

public class NewsActivity extends AppCompatActivity implements ViewInterface, TopNewsinterface {

    private NewsPresenter newsPresenter;
    private NewsAdapter newsAdapter;

    private static String keyword = "android";

    private static String keywordAll = "all";

    private int page = 20;

    RecyclerView recyclerView;

    LinearLayout emptyStateLayout;

    LinearLayout errorStateLayout;

    EditText uiViewSearch;

    ProgressBar uiViewProgressBar;

    ImageView searchNews;

    Button uiViewButtonRetry;

    SliderLayout sliderLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);

        recyclerView = findViewById(R.id.recyclerView);
        searchNews = findViewById(R.id.img_search_news);
        uiViewSearch = findViewById(R.id.ui_view_search);
        uiViewProgressBar = findViewById(R.id.ui_view_progressbar);
        emptyStateLayout = findViewById(R.id.ui_view_empty_state);
        errorStateLayout = findViewById(R.id.ui_view_error_state);
        uiViewButtonRetry = findViewById(R.id.ui_view_button_retry);
        sliderLayout = findViewById(R.id.imageSlider);

        newsPresenter = new NewsPresenter(this,this);
        newsAdapter = new NewsAdapter(null);

        recyclerView.setAdapter(newsAdapter);

        sliderLayout.setIndicatorAnimation(SliderLayout.Animations.FILL);

        checkInternetConnection();
        setListener();
    }

    private void checkInternetConnection(){
        if(CheckConnectionInternet.isConnectedToNetwork(this)){
            newsPresenter.getEverything(keyword,page);
            newsPresenter.getTopNews(keywordAll,5);
        } else {
            newsPresenter.getListArticleFromLocalDb();
        }
    }
    private void setListener(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1)){
                    getMoreData(page++);
                }
            }
        });

        searchNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsPresenter.getEverything(uiViewSearch.getText().toString(), 20);
            }
        });

        uiViewButtonRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressBar();
                newsPresenter.getEverything(keyword,page);
            }
        });
    }
    @Override
    public void onSuccessGetNews(List<Articles> articleList) {
        hideProgressBar();
        if(articleList.size() > 0) {
            newsPresenter.setArticletoLocalDb(articleList);;
            newsAdapter.setArticleList(articleList);
            newsAdapter.notifyDataSetChanged();
        } else {
            setEmptyState();
        }

    }

    @Override
    public void onErrorGetNews(Throwable throwable) {
        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_LONG).show();
        setErrorState();
    }

    @Override
    public void onLoading(){
        showProgressBar();
    }

    @Override
    public void onSuccessTopNews(List<Articles> articlesList){
        if(articlesList.size() > 0){

            Integer totalArticles = articlesList.size();

            for(int i=0;i<totalArticles; i++){
                SliderView sliderView = new SliderView(this);
                sliderView.setImageUrl(articlesList.get(i).getUrlToImage());
                sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
                sliderLayout.addSliderView(sliderView);
            }

        } else {
            hideProgressBar();
        }
    }

    @Override
    public void onErrorTopNews(Throwable throwable){
        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void setEmptyState(){
        emptyStateLayout.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        errorStateLayout.setVisibility(View.GONE);
    }

    private void setErrorState(){
        errorStateLayout.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        emptyStateLayout.setVisibility(View.GONE);
        uiViewProgressBar.setVisibility(View.GONE);
    }

    private void getMoreData(int scroll){
        if(uiViewSearch.getText().toString() != ""){
            newsPresenter.getEverything(uiViewSearch.getText().toString(), 20);
        } else {
            newsPresenter.getEverything(keyword, scroll);
        }
    }


    private void showProgressBar(){
        uiViewProgressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        errorStateLayout.setVisibility(View.GONE);
        emptyStateLayout.setVisibility(View.GONE);
    }

    private void hideProgressBar(){
        uiViewProgressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        newsPresenter.unsubscribe();
    }
}
