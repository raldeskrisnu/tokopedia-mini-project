package com.tokopedia.testproject.problems.news.presenter;

import com.tokopedia.testproject.problems.news.model.Articles;
import com.tokopedia.testproject.problems.news.model.NewsResult;
import com.tokopedia.testproject.problems.news.network.NewsDataSource;

import com.tokopedia.testproject.problems.news.viewInterface.TopNewsinterface;
import com.tokopedia.testproject.problems.news.viewInterface.ViewInterface;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hendry on 27/01/19.
 */
public class NewsPresenter implements NewsPresenterInterface {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private ViewInterface viewInterface;

    private TopNewsinterface topNewsinterface;

    private static String SORT_BY = "publishedAt";

    private Realm realm = Realm.getDefaultInstance();


    public NewsPresenter(ViewInterface viewInterface, TopNewsinterface topNewsinterface) {
        this.viewInterface = viewInterface;
        this.topNewsinterface = topNewsinterface;
    }

    @Override
    public void getEverything(String keyword, int page) {
        viewInterface.onLoading();
        NewsDataSource.getService().getEverythingService(keyword,page,SORT_BY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NewsResult>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(NewsResult newsResult) {
                        viewInterface.onSuccessGetNews(newsResult.getArticlesList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        viewInterface.onErrorGetNews(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void getTopNews(String keyword, int page){

        NewsDataSource.getService().getTopNewsService(keyword,page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NewsResult>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(NewsResult newsResult) {
                        topNewsinterface.onSuccessTopNews(newsResult.getArticlesList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        topNewsinterface.onErrorTopNews(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void setArticletoLocalDb(List<Articles> articlesList){
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for(Articles articles : articlesList){
                        realm.insert(articles);
                        realm.beginTransaction();
                        realm.commitTransaction();
                    }

                }
            });
        } catch (Exception e){
            if (realm != null) {
                if (realm.isInTransaction()) {
                    realm.cancelTransaction();
                }
            }
        }
    }

    @Override
    public void getListArticleFromLocalDb(){
        List<Articles> listArticle = new ArrayList<>();
        try {
            listArticle = realm.where(Articles.class).findAll();
        }catch (Exception e){
            viewInterface.onErrorGetNews(e);
        }

        viewInterface.onSuccessGetNews(listArticle);
    }

    public void unsubscribe() {
        compositeDisposable.dispose();
    }
}
