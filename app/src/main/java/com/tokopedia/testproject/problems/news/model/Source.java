package com.tokopedia.testproject.problems.news.model;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;

public class Source extends RealmObject {

    @SerializedName("id")
    String id;

    @SerializedName("name")
    String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
