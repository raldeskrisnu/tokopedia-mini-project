package com.tokopedia.testproject.problems.algorithm.waterJug;

public class Solution {

    public static int minimalPourWaterJug(int jug1, int jug2, int target) {
        // TODO, return the smallest number of POUR action to do the water jug problem
        // below is stub, replace with your implementation!

        if (target > jug2)
        {
            return -1;
        }


        if ((target % checkJug(jug2, jug1)) != 0)
        {
            return -1;
        }


        return Math.min(checkPour(jug2, jug1, target), checkPour(jug1, jug2, target));
    }

    private static int checkJug(int jug2, int jug1)
    {
        if (jug1 == 0)
        {
            return jug2;
        }
        return checkJug(jug1, jug2 % jug1);
    }

    private static int checkPour(int fromPour, int toPour, int target)
    {
        int from = fromPour;
        int to = 0;
        int litre = 1;

        while (from != target && to != target) {
            int temp = Math.min(from, toPour - to);
            to += temp;
            from -= temp;
            litre++;

            if (from == target || to == target) {
                break;
            }

            if (from == 0) {
                from = fromPour;
                litre++;
            }

            if (to == fromPour) {
                to = 0;
                litre++;
            }
        }

        return litre;
    }

}
