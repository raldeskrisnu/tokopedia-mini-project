package com.tokopedia.testproject.problems.algorithm.maxrectangle;


import java.util.ArrayList;
import java.util.Stack;

public class Solution {

    public static int maxRect(int[][] matrix) {

        if(matrix == null || matrix.length == 0 || matrix[0].length == 0){
            return 0;
        }
        else {
            int[] line = new int[matrix[0].length];

            for (int i = 0; i < matrix[0].length; i++) {
                line[i] = matrix[0][i];
            }

            int result = getLargestArea(line);

            for (int i = 1; i < matrix.length; i++) {
                updatedLine(matrix, line, i);
                result = Math.max(result, getLargestArea(line));
            }
            return result;
        }
    }

    public static int getLargestArea(int[] line){
        if(line == null || line.length == 0) {
            return 0;
        }

        int lineLength = line.length;
        Stack<Integer> integerStack = new Stack<>();

        int area = 0;

        for(int i = 0; i <= lineLength; i++){
            int h = i == line.length ? 0 : line[i];
            if(integerStack.isEmpty() || h >= line[integerStack.peek()]){
                integerStack.push(i);
            }
            else{
                int temp = integerStack.pop();
                area = Math.max(area, line[temp] * (integerStack.isEmpty() ? i : i - 1 - integerStack.peek()));
                i--;
            }
        }

        return area;
    }

    public static void updatedLine(int[][] matrix, int[] line, int index){
        for(int i = 0; i < matrix[0].length; i ++){
            line[i] = matrix[index][i] == 1 ? line[i] + 1 : 0;
        }
    }
}
