package com.tokopedia.testproject.problems.androidView.graphTraversal;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tokopedia.testproject.R;

import de.blox.graphview.*;
import de.blox.graphview.energy.FruchtermanReingoldAlgorithm;

import java.util.ArrayList;
import java.util.List;

public class GraphActivity extends AppCompatActivity {
    private int nodeCount = 1;
    private Node currentNode;
    protected BaseGraphAdapter<ViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        final Graph graph = createGraph();
        setupAdapter(graph);
    }

    private void setupAdapter(Graph graph) {

        traverseAndColorTheGraph(graph, graph.getNode(0), 2);

        final GraphView graphView = findViewById(R.id.graph2);

        adapter = new BaseGraphAdapter<ViewHolder>(this, R.layout.node, graph) {
            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(View view) {
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(ViewHolder viewHolder, Object data, int position) {

                int[] color = {
                        //ungu
                        Color.rgb(128, 0, 128),
                        Color.GREEN,
                        //oranye
                        Color.rgb(255, 165, 0),
                        Color.BLUE
                };

                viewHolder.textView.setText(data.toString());

                viewHolder.cardView.setBackgroundColor(color[0]);

                if (data.toString().equals(node.data.toString())) {
                    viewHolder.cardView.setBackgroundColor(color[3]);
                }

                for (int i = 0; i < listNode.size(); i++) {

                    for (int j = 0; j < listNode.get(i).size(); j++) {

                        if (data.toString().equals(listNode.get(i).get(j).data.toString())) {
                            viewHolder.cardView.setBackgroundColor(color[i + 1]);
                        }
                    }
                }


            }
        };

        adapter.setAlgorithm(new FruchtermanReingoldAlgorithm());

        graphView.setAdapter(adapter);
        graphView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentNode = adapter.getNode(position);
                adapter.notifyDataChanged(currentNode);
                Snackbar.make(graphView, "Clicked on " + currentNode.getData().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        /*
         * TODO
         * Given input:
         * 1. a graph that represents a tree (there is no cyclic node),
         * 2. a rootNode, and
         * 3. a target distance,
         * you have to traverse the graph and give the color to each node as below criteria:
         * 1. RootNode is purple
         * 2. Nodes with the distance are less than the target distance are colored green
         * 3. Nodes with the distance are equal to the target distance are colored orange
         * 4. Other Nodes are blue
         */

        traverseAndColorTheGraph(graph, graph.getNode(0), 2);
    }

    private ArrayList<ArrayList<Node>> listNode = new ArrayList<>();
    private Node node;

    private void traverseAndColorTheGraph(Graph graph, Node rootNode, int target) {
        List<Edge> allNode = graph.getEdges();
        Node getRootNode = rootNode;
        node = rootNode;

        listNode.clear();

        for (int i = 0; i < target; i++) {
            ArrayList<Node> childRoot = new ArrayList<>();

            if (listNode.size() > 0) {
                for (int j = 0; j < listNode.get(i - 1).size(); j++) {
                    getRootNode = listNode.get(i - 1).get(j);
                    for (Edge e : allNode) {
                        if (e.getSource() == getRootNode) {
                            childRoot.add(e.getDestination());
                        }
                    }
                }
            } else {
                for (Edge e : allNode) {
                    if (e.getSource() == getRootNode) {
                        childRoot.add(e.getDestination());
                    }
                }
            }

            listNode.add(childRoot);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public Graph createGraph() {
        final Graph graph = new Graph();
        final Node a = new Node(getNodeText());
        final Node b = new Node(getNodeText());
        final Node c = new Node(getNodeText());
        final Node d = new Node(getNodeText());
        final Node e = new Node(getNodeText());
        final Node f = new Node(getNodeText());
        final Node g = new Node(getNodeText());

        graph.addEdge(a, b);
        graph.addEdge(a, c);
        graph.addEdge(b, f);
        graph.addEdge(b, g);
        graph.addEdge(c, d);
        graph.addEdge(c, e);
        return graph;
    }

    private class ViewHolder {
        TextView textView;
        LinearLayout bgChanged;
        CardView cardView;

        ViewHolder(View view) {
            textView = view.findViewById(R.id.textView);
            bgChanged = view.findViewById(R.id.backgroud);
            cardView = view.findViewById(R.id.card_view);
        }
    }

    protected String getNodeText() {
        return "Node " + nodeCount++;
    }
}
