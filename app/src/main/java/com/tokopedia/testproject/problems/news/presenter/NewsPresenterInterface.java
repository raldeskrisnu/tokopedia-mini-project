package com.tokopedia.testproject.problems.news.presenter;

import com.tokopedia.testproject.problems.news.model.Articles;

import java.util.List;

public interface NewsPresenterInterface {

    void getEverything(String keyword, int page);

    void getTopNews(String keyword, int page);

    void setArticletoLocalDb(List<Articles> articlesList);

    void getListArticleFromLocalDb();
}
