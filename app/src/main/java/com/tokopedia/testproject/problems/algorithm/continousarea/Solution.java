package com.tokopedia.testproject.problems.algorithm.continousarea;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by hendry on 18/01/19.
 */
public class Solution {

    static int row = 0;

    static int column = 0;

    static int count = 0;

    static ArrayList<Integer> integerArrayList = new ArrayList<>();

    public static int maxContinuousArea(int[][] matrix) {
        // TODO, return the largest continuous area containing the same integer, given the 2D array with integers
        // below is stub

        row = matrix.length;
        column = matrix[0].length;

        if(column > row){
            int tempColumn = column;
            column = row;
            row = tempColumn;
        } else {
            getMatrix(matrix,column,row);
        }

        int result = 0;
        for (int i = 0; i < integerArrayList.size(); i++) {
            if (integerArrayList.get(i) > result) {
                result = integerArrayList.get(i);
            }
        }

        Log.d("result","end result" + result);

        return result;

    }

    private static void getMatrix(int[][] matrix, int column, int row){
        for(int i = 0; i < column; i++){
            for(int k = 0; k < row; k++){
                addArrayList(matrix, i, k, column, row);
            }
        }
    }

    private static void check(int[][] matrix, int x, int y, int prevC, int a, int b){

        if (x < 0 || x >= a || y < 0 || y >= b) {
            return;
        }
        if (matrix[x][y] != prevC) {
            return;
        }
        matrix[x][y] = 99;

        count = count + 1;
        check(matrix, x + 1, y, prevC, a, b);
        check(matrix, x - 1, y, prevC, a, b);
        check(matrix, x, y + 1, prevC, a, b);
        check(matrix, x, y - 1, prevC, a, b);
    }

    private static void addArrayList(int[][] screen, int x, int y, int ncolumn, int nrow) {
        count = 0;
        int[][] target = new int[row][column];
        for (int i = 0; i < row; i++) {
            System.arraycopy(screen[i], 0, target[i], 0, screen[i].length);
        }

        int prevC = target[x][y];
        check(target, x, y, prevC, ncolumn, nrow);
        integerArrayList.add(count);
    }


}
