package com.tokopedia.testproject.problems.news.viewInterface;

import com.tokopedia.testproject.problems.news.model.Articles;

import java.util.List;

public interface ViewInterface {

    void onSuccessGetNews(List<Articles> articleList);

    void onErrorGetNews(Throwable throwable);

    void onLoading();
}
